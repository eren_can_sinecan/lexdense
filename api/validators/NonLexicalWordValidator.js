'use strict'
const InvalidInputError = require('../entities/error/InvalidInputError')

class NonLexicalWordValidator {
  /**
     * Word length can't be more than text length
     * @param word
     */
  static validateWordLength (word) {
    if (word.length >= 1000) {
      throw new InvalidInputError()
    }
  }

  static validateAlphabetic (word) {
    if (!/^[a-zA-Z()]+$/.test(word)) {
      throw new InvalidInputError()
    }
  }
}
module.exports = NonLexicalWordValidator
