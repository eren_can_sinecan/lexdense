'use strict'
const InvalidInputError = require('../entities/error/InvalidInputError')

class ComplexityCheckValidator {
  static validateTextLength (text) {
    if (text.length >= 1000) {
      throw new InvalidInputError()
    }
  }

  static validateWordCount (text) {
    if (text.split(' ').length >= 100) {
      throw new InvalidInputError()
    }
  }
}
module.exports = ComplexityCheckValidator
