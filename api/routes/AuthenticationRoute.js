'use strict'
const ResponseSender = require('./util/ResponseSender')
const AuthService = require('../services/AuthService')
const authService = new AuthService()

const postAuthentication = async (req, res) => {
  try {
    let { passphrase } = req.swagger.params.authentication.value
    let responseBody = await authService.issueToken(passphrase)
    ResponseSender.sendResponse(res, responseBody)
  } catch (e) {
    ResponseSender.sendResponse(res, e)
  }
}

module.exports = {
  postAuthentication: postAuthentication
}
