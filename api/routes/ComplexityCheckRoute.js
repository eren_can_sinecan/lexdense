'use strict'
const ResponseSender = require('./util/ResponseSender')
const ComplexityCheckValidator = require('../validators/ComplexityCheckValidator')
const ComplexityCheckService = require('../services/ComplexityCheckService')
const complexityCheckService = new ComplexityCheckService()

const calculateDensity = async (req, res) => {
  try {
    let { lexicalInput, mode } = req.swagger.params
    let text = lexicalInput.value.text
    let isVerbose = (!!mode.value) && (mode.value === 'verbose')
    ComplexityCheckValidator.validateTextLength(text)
    ComplexityCheckValidator.validateWordCount(text)
    let responseBody = await complexityCheckService.calculateDensity(text, isVerbose)
    ResponseSender.sendResponse(res, responseBody)
  } catch (e) {
    ResponseSender.sendResponse(res, e)
  }
}

module.exports = {
  calculateDensity: calculateDensity
}
