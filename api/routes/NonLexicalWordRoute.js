'use strict'
const ResponseSender = require('./util/ResponseSender')
const NonLexicalWordValidator = require('../validators/NonLexicalWordValidator')
const NonLexicalWordService = require('../services/NonLexicalWordService')
const nonLexicalWordService = new NonLexicalWordService()

const postNonLexicalWord = async (req, res) => {
  try {
    let word = req.swagger.params.nonLexicalWordInput.value.word
    NonLexicalWordValidator.validateAlphabetic(word)
    NonLexicalWordValidator.validateWordLength(word)
    let responseBody = await nonLexicalWordService.postNonLexicalWord(word)
    ResponseSender.sendResponse(res, responseBody)
  } catch (e) {
    ResponseSender.sendResponse(res, e)
  }
}

module.exports = {
  postNonLexicalWord: postNonLexicalWord
}
