'use strict'
const RuntimeError = require('../../entities/error/RuntimeError')

class ResponseSender {
  /**
     * A shorthand method to send responses to the client. Obscures the error message if it's not sent deliberately.
     * @param res
     * @param serviceResponse
     */
  static sendResponse (res, serviceResponse) {
    if (serviceResponse instanceof Error) {
      if (!serviceResponse.status || isNaN(serviceResponse.status)) {
        // Not one of our custom errors. We don't want to expose inner mechanics
        res.status(500).json(new RuntimeError())
      } else {
        res.status(serviceResponse.status).json(serviceResponse)
      }
    } else {
      res.status(200).json(serviceResponse)
    }
  }
}

module.exports = ResponseSender
