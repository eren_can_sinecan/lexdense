'use strict'
const { findNonLexicalWord } = require('../entities/model/WordDAO')
const RuntimeError = require('../entities/error/RuntimeError')

class ComplexityCheckService {
  /**
     * * I thought of two approaches. One is loading the list of nonlexical words at
     * the application start and caching them, updating only when there's a new word added.
     * However, I'll take the approach of querying each word individually in
     * our nonlexical word database.
     * @param text
     * @param isVerbose
     * @returns {Promise<void>}
     */
  async calculateDensity (text, isVerbose) {
    try {
      let lexicalDensity = {}
      lexicalDensity.overall_ld = await this.getLexicalDensity(text)

      if (isVerbose) {
        let sentence_ldPromises = text.split('.').map(async (sentence) => {
          let sentenceLd = await this.getLexicalDensity(sentence)
          return sentenceLd
        })
        lexicalDensity.sentence_ld = await Promise.all(sentence_ldPromises)
      }
      return lexicalDensity
    } catch (e) {
      console.error(e)
      throw new RuntimeError()
    }
  }

  /**
     * Removes punctuation from text except for apostrophe because it can occur mid-word (i.e. "can't"), case insensitively
     * searches for each word on the nonlexical words database, returns lexical density percentage.
     * @param text
     * @returns {Promise<void>}
     */
  async getLexicalDensity (text) {
    const textAsWords = text.toLowerCase().match(/\w+(?:'\w+)*/g)
    const totalWordCount = textAsWords.length
    const occurences = this.getOccurences(textAsWords)
    let nonLexicalCountPromises =
    Object.keys(occurences).map(async (word) => {
      if (!(/\d/.test(word))) { // I'm assuming any word that contains numbers would be lexical such as "3rd" or "c3po"
        let nonLexical = await this.isNonLexical(word)
        if (nonLexical) {
          return occurences[word]
        } else {
          return 0
        }
      }
    })

    return Promise.all(nonLexicalCountPromises).then(nonLexicalCounts => {
      const sumReducer = (accumulator, currentValue) => accumulator + currentValue
      let nonLexicalSum = nonLexicalCounts.reduce(sumReducer)
      let result = ((totalWordCount - nonLexicalSum) / totalWordCount) * 100
      return Number(Math.round(result + 'e2') + 'e-2')
    })
  }

  /**
     *
     * @param word
     * @returns {Promise<boolean>}
     */
  async isNonLexical (word) {
    const matchingNonLexicals = await findNonLexicalWord(word)
    if ((!!matchingNonLexicals) && matchingNonLexicals.length) {
      return true
    }
    return false
  }

  /**
     * Counts number of occurences of each word. This way we don't have to re-query repeated occurrences of the words
     * in the given sentence.
     * @param text
     * @private
     */
  getOccurences (textAsWords) {
    return textAsWords.reduce((accumulator, currentValue) => {
      !accumulator[currentValue] ? accumulator[currentValue] = 1 : accumulator[currentValue]++
      return accumulator
    }, {})
  }
}
module.exports = ComplexityCheckService
