'use strict'
const jwt = require('jsonwebtoken')

class JWTUtils {
  async verify (token, privateKey) {
    return new Promise((resolve, reject) => { // Wrapping function with callback into promise.
      jwt.verify(token, privateKey, (verificationError, decodedToken) => {
        if (verificationError) {
          reject(verificationError)
        } else {
          resolve(decodedToken)
        }
      })
    })
  }

  async sign (profile, privateKey) {
    return new Promise((resolve, reject) => {
      jwt.sign(profile, privateKey, { expiresIn: '1h' }, (err, token) => {
        if (err) {
          reject(err)
        } else {
          resolve(token)
        }
      })
    })
  }
}

module.exports = JWTUtils
