'use strict'
const { saveNonLexicalWord } = require('../entities/model/WordDAO')
const RuntimeError = require('../entities/error/RuntimeError')

class NonLexicalWordService {
  async postNonLexicalWord (word) {
    try {
      await saveNonLexicalWord(word)
      return word
    } catch (e) {
      console.error(e)
      throw new RuntimeError()
    }
  }
}

module.exports = NonLexicalWordService
