'use strict'
const UnauthorizedError = require('../entities/error/UnauthorizedError')
const ResponseSender = require('../routes/util/ResponseSender')

const PASSPHRASE = 'secret passphrase'
const privateKey = '1234'
const JWTUtils = require('./util/JWTUtils')

class AuthService {
  static async verifyToken (req, authOrSecDef, token, callback) {
    const jwt = new JWTUtils()
    try {
      let decodedToken = await jwt.verify(token, privateKey)

      if (decodedToken) {
        return callback(null)
      } else {
        throw new UnauthorizedError()
      }
    } catch (e) {
      console.error(e)
      if (e.message === 'invalid signature') {
        ResponseSender.sendResponse(req.res, new UnauthorizedError())
      }
    }
  }

  async issueToken (passphrase) {
    const jwt = new JWTUtils()
    try {
      let userProfile = this.queryUserProfileByPassphrase(passphrase)
      let token = await jwt.sign(userProfile, privateKey)
      return token
    } catch (e) {
      throw new UnauthorizedError()
    }
  }

  /**
     * Deals with pseudoconfirmation of user. ordinarily, we would have both the username and the password. We would
     * retrieve the user by the username, compare hash of the submitted passphrase with the password kept on the profile
     * which would be hash of the password submitted during registration. But all that would be an unnecessary expansion
     * of the scope, so let's mock it like this instead.
     * @param passphrase
     * @returns {{foo: string}}
     */
  queryUserProfileByPassphrase (passphrase) {
    if (!passphrase || passphrase !== PASSPHRASE) {
      throw new UnauthorizedError()
    }

    return { foo: 'bar' } // mock user profile
  }
}

module.exports = AuthService
