'use strict'

let { nonLexicalWords } = require('../../../config/initialValues')

const mongoose = require('mongoose')
let WordModel

module.exports.initModel = () => {
  mongoose
    .connect(
      'mongodb://mongo/express-mongo',
      { useNewUrlParser: true }
    )
    .then(() => {
      console.log('Word DB Connected')
      const WordSchema = new mongoose.Schema({
        label: {
          type: String,
          required: true,
          trim: true,
          index: true // Though single field schemas are autoindexed, it's nice to be explicit.
        }
      })
      WordModel = mongoose.model('word', WordSchema)

      if ((!!nonLexicalWords) && nonLexicalWords instanceof Array) {
        nonLexicalWords.forEach(async (word) => {
          try {
            await saveNonLexicalWord(word)
            console.log('added to db: ' + word)
          } catch (e) {
            console.error('error while saving: ' + word)
            console.error(e)
          }
        })
      }
    })
    .catch(err => console.log(err))
}

/**
 * We expose a function rather than the mongoose model itself in case we change our db
 * @param word
 * @returns {Promise<*>}
 */
module.exports.findNonLexicalWord = async (word) => {
  return WordModel.find({ 'label': word.trim().toLowerCase() }).exec()
}

const saveNonLexicalWord = module.exports.saveNonLexicalWord = async (word) => {
  let newWord = new WordModel({
    label: word
  })
  return newWord.save()
}
