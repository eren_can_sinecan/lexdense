'use strict'
const BaseError = require('./BaseError')

class NotFoundError extends BaseError {
  constructor () {
    super(500, 'Not Found')
  }
}

module.exports = NotFoundError
