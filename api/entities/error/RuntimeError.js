'use strict'
const BaseError = require('./BaseError')

class RuntimeError extends BaseError {
  constructor () {
    super(500, 'Internal Server Error')
  }
}

module.exports = RuntimeError
