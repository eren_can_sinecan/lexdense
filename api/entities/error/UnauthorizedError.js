'use strict'
const BaseError = require('./BaseError')

class UnauthorizedError extends BaseError {
  constructor () {
    super(401, 'Unauthorized')
  }
}

module.exports = UnauthorizedError
