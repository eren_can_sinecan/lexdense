'use strict'
const BaseError = require('./BaseError')

class InvalidInputError extends BaseError {
  constructor () {
    super(400, 'Invalid Input')
  }
}

module.exports = InvalidInputError
