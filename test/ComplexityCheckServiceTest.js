const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
const { before, describe } = require('mocha')
const mongoose = require('mongoose')
const Mockgoose = require('mock-mongoose').Mockgoose
const mockgoose = new Mockgoose(mongoose)

const WordDAO = require('../api/entities/model/WordDAO')
const ComplexityCheckService = require('../api/services/ComplexityCheckService')
let complexityCheckService

chai.use(chaiAsPromised)
chai.should()

before(function (done) {
  mockgoose.prepareStorage().then(async function () {
    try {
      await WordDAO.initModel()
      complexityCheckService = new ComplexityCheckService()
    } catch (e) {
      done(e)
    }
  })
})

describe('ComplexityCheckService', () => {
  it('should return correct density for valid input', done => {
    complexityCheckService.calculateDensity('ash to ash. dust to dust. fade to black', true)
      .should.be.fulfilled.then(response => {
        console.log(JSON.stringify(response))
        response.overall_ld.should.equal(66.67)
        response.sentence_ld.should.equal([66.67, 66.67, 66.67])
      }).should.notify(done)
  })
})
