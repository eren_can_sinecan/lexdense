FROM node:10
WORKDIR /app
COPY package.json package.json
RUN npm install
COPY . .
EXPOSE 8093
RUN npm install -g nodemon
CMD ["node", "--inspect", "app.js"]