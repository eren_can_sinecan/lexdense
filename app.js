// 3rd party imports
const bodyParser = require('body-parser')
const swaggerTools = require('swagger-tools')
const jsyaml = require('js-yaml')
const http = require('http')
const App = require('express')
const fs = require('fs')
const model = require('./api/entities/model/WordDAO')

// Imports from the app
const ResponseSender = require('./api/routes/util/ResponseSender')
const NotFoundError = require('./api/entities/error/NotFoundError')
const AuthService = require('./api/services/AuthService')

try {
  model.initModel()
  const app = App()
  app.use(bodyParser.json({ type: ['application/json', 'application/*+json'] }))

  const swaggerDoc = jsyaml.safeLoad(fs.readFileSync(__dirname + '/api/swagger/swagger.yaml', 'utf8'))

  swaggerTools.initializeMiddleware(swaggerDoc, (swaggerMiddleware) => {
    // TODO post mortem debug

    app.use(swaggerMiddleware.swaggerMetadata())

    app.use(swaggerMiddleware.swaggerSecurity({
      APIKey: AuthService.verifyToken
    }))

    app.use(swaggerMiddleware.swaggerValidator())

    const routerConfig = {
      swaggerUi: swaggerDoc,
      controllers: __dirname + '/api/routes',
      useStubs: false
    }

    app.use(swaggerMiddleware.swaggerRouter(routerConfig))

    app.use(swaggerMiddleware.swaggerUi())

    app.use('/*', (req, res, next) => {
      ResponseSender.sendResponse(res, new NotFoundError())
    })

    http.createServer(app)
      .listen(8093, () => {
        console.warn('Your server is listening on: http://localhost:8093')
        console.log('Swagger-ui is available on:  http://localhost:8093/docs')
      })
  })

  /**
     * Absolute last resort. Listener on process. Otherwise we handle the errors in place
     */
  process.on('uncaughtException', (err) => {
    console.error('CRITICAL ERROR: ', err)
  })
  process.on('unhandledRejection', (reason, p) => {
    console.log('Unhandled Rejection at: Promise', p, 'reason:', reason)
    Error.captureStackTrace(p)
    console.log('Unhandled Rejection Stack Trace: \n' + p.stack)
  })
} catch (err) {
  console.error('CRITICAL ERROR: ', err)
}
