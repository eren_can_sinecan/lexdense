Lexical density checker:

checks for lexical density based on a list of words found in ./config folder. 
To run: in the root directory, run "docker-compose up --build"
Then I recommend navigating to http://localhost:8093/docs for tests. There's a Swagger interface with descriptions of endpoints there, it's way easier than doing the same with Postman or the like.
